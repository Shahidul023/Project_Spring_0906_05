<?php

$myFloatValue= floatval ("10000 this is a string");
echo $myFloatValue."<br>";

$myVar=-0.0000001;
if (empty($myVar)){

    echo "$myVar is empty<br><br>";
}
else {
    echo "$myVar is not empty<br><br>";
}

//serialize unserialize start
$myArray    =array("Bangladesh","New Zealand", "Tamil", 100, 342.34);
$mySerializedValue = serialize($myArray);
echo $mySerializedValue. "<br>";

$unknownVar = unserialize($mySerializedValue);
echo "<pre>";
var_export ($unknownVar);
echo "</pre>";

//serialize unserialize stops here